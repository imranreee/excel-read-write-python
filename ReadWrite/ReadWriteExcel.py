import openpyxl

excel_path = "Input.xlsx"
wb = openpyxl.load_workbook(excel_path)

quarterly_sheet = wb['Quarterly']
monthly_sheet = wb['Monthly']
yearly_sheet = wb['Yearly']

# For transferring data from monthly to quarterly
number_of_column = monthly_sheet.max_column
number_of_row = monthly_sheet.max_row

mod_row = number_of_row % 3
number_of_row_new = number_of_row - mod_row
number_of_row_dev = int((number_of_row_new / 3))

# print(number_of_column)
# print(number_of_row)
# print(mod_row)
# print(number_of_row_new)
# print(number_of_row_dev)

for k in range(2, number_of_column + 1):
    row_range_start = 1
    row_range_end = 4
    for i in range(1, number_of_row_dev + 1):
        sum_of_q = 0

        for j in range(row_range_start, row_range_end):
            sum_of_q += monthly_sheet.cell(j, k).value
        row_range_start += 3
        row_range_end += 3
        # print(sum_of_q)
        quarterly_sheet.cell(i, k, sum_of_q)

# For transferring data quarterly to yearly
number_of_row_y = yearly_sheet.max_row
number_of_column_y = yearly_sheet.max_column

mod_row_q = number_of_row_dev % 4
number_of_row_new_q = number_of_row_dev - mod_row_q
number_of_row_dev_q = int((number_of_row_new_q / 4))

for a in range(2, number_of_column + 1):
    row_range_start_q = 1
    row_range_end_q = 5
    for b in range(1, number_of_row_dev_q + 1):
        sum_of_y = 0

        for c in range(row_range_start_q, row_range_end_q):
            sum_of_y += quarterly_sheet.cell(c, a).value
        row_range_start_q += 4
        row_range_end_q += 4
        # print(sum_of_y)
        yearly_sheet.cell(b, a, sum_of_y)

all_quarter = []
all_years = []

for i in range(1, number_of_row + 1):
    value_m = monthly_sheet.cell(i, 1).value
    years = int(str(value_m)[:4])
    months = int(str(value_m)[-2:])
    # print(years)
    # print(months)

    all_years.append(str(years))

    if months <= 3:
        all_quarter.append(str(years)+" Q1")

    elif 4 <= months <= 6:
        all_quarter.append(str(years)+" Q2")

    elif 7 <= months <= 9:
        all_quarter.append(str(years)+" Q3")

    elif 10 <= months <= 12:
        all_quarter.append(str(years)+" Q4")

marge_quarter = []
marge_years = []

for i in range(len(all_quarter)):
    # print(all_quarter[i])
    if all_quarter[i] in marge_quarter:
        print("Working...")
        # print(all_quarter[i] + " already exist")
    else:
        marge_quarter.append(all_quarter[i])

    if all_years[i] in marge_years:
        print("Working...")
        # print(all_years[i] + " already exist")
    else:
        marge_years.append(all_years[i])


for i in range(len(marge_quarter)):
    # print(marge_quarter[i])
    quarterly_sheet.cell(i + 1, 1, marge_quarter[i])


for i in range(len(marge_years)):
    # print(marge_years[i])
    yearly_sheet.cell(i + 1, 1, marge_years[i])

wb.save(excel_path)
print("****** Reading and Writing Done ******")

